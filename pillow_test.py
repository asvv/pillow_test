import sys

from PIL import Image
from PIL.ExifTags import TAGS

from core import exif_info


filename = sys.argv[1]
extension = filename.split('.')[-1]

if (extension == 'jpg') | (extension == 'JPG') | (extension == 'jpeg') | (extension == 'JPEG'):
    try:
        img = Image.open(filename)
        info = img._getexif()
        exif = {}
        for tag, value in info.items():
            decoded = TAGS.get(tag, tag)
            exif[decoded] = value

        exif_info = exif_info.ExifInfo(exif)
        exif_info.calculate_all()
        exif_info.print_info()

    except:
        print("this picture does not have Exif data")
        pass
